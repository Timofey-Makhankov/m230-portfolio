public class Main {
    public static void main(String[] args) {
        GeometryCalculations geometryCalculations = new GeometryCalculations(new Triangle(3, 4, 5));
        System.out.printf("Area: %.3f%n", geometryCalculations.triangleArea());
        System.out.printf("Perimeter: %.2f%n", geometryCalculations.trianglePerimeter());
        new Triangle(1, 4, 7);
    }
}