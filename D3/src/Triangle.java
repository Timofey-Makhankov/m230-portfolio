public class Triangle {
    private final double sideA;
    private final double sideB;
    private final double sideC;

    private final double height;

    public Triangle(double sideA, double sideB, double sideC) throws InvalidTriangleException {
        if (!validTriangle(sideA, sideB, sideC)) {
            throw new InvalidTriangleException(
                    String.format("The given Triangle sides [a=%f, b=%f, c=%f] are invalid", sideA, sideB, sideC)
            );
        }
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;
        this.height = getHeight();
    }

    public double getHeight() {
        return (0.5 / sideB)
                * Math.sqrt(sideA + sideB + sideC)
                * Math.sqrt(-sideA + sideB + sideC)
                * Math.sqrt(sideA - sideB + sideC)
                * Math.sqrt(sideA + sideB - sideC);
    }

    private boolean validTriangle(double a, double b, double c) {
        return a + b > c && a + c > b && b + c > a;
    }

    public double triangleArea() {
        return height * sideB / 2;
    }

    public double trianglePerimeter() {
        return sideA + sideB + sideC;
    }
}
