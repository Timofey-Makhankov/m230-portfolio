public class InvalidTriangleException extends IllegalArgumentException {
    public InvalidTriangleException(){
        super();
    }

    public InvalidTriangleException(String message){
        super(message);
    }
}
