public class GeometryCalculations {
    private final Triangle triangle;

    public GeometryCalculations(Triangle triangle){
        this.triangle = triangle;
    }

    public double triangleArea() {
        return triangle.triangleArea();
    }

    public double trianglePerimeter() {
        return triangle.trianglePerimeter();
    }

    public Triangle getTriangle() {
        return triangle;
    }
}
