public class Heater {
    private int currentTemperature;
    private int minTemperature;
    private int maxTemperature;
    private int incrementSize;

    public Heater() {
        this(15, 5, 15, 15);
    }

    public Heater(int currentTemperature, int incrementSize) {
        this(currentTemperature, incrementSize, 15, 15);
    }

    public Heater(int currentTemperature, int incrementSize, int minTemperature, int maxTemperature) {
        this.maxTemperature = maxTemperature;
        this.incrementSize = incrementSize;
        this.minTemperature = minTemperature;
        this.currentTemperature = currentTemperature;
    }

    @Override
    public String toString() {
        return String.format(
                "{c.temp: %d, min.temp: %d, max.temp: %d, temp. step: %d}",
                this.currentTemperature,
                this.minTemperature,
                this.maxTemperature,
                this.incrementSize
        );
    }

    // Getters
    public int getCurrentTemperature() {
        return currentTemperature;
    }

    public int getIncrementSize() {
        return incrementSize;
    }

    public int getMaxTemperature() {
        return maxTemperature;
    }

    public int getMinTemperature() {
        return minTemperature;
    }

    // Setters
    public void setCurrentTemperature(int currentTemperature) {
        this.currentTemperature = currentTemperature;
    }

    public void setIncrementSize(int incrementSize) {
        this.incrementSize = incrementSize;
    }

    public void setMaxTemperature(int maxTemperature) {
        this.maxTemperature = maxTemperature;
    }

    public void setMinTemperature(int minTemperature) {
        this.minTemperature = minTemperature;
    }
}
