public class Main {
    public static void main(String[] args) {
        System.out.println(new Heater());
        System.out.println(new Heater(25, 2));
        System.out.println(new Heater(10, 3, 5, 25));
        Heater heater = new Heater();
        heater.setCurrentTemperature(20);
        heater.setMaxTemperature(100);
        heater.setMinTemperature(-30);
        heater.setIncrementSize(10);
        System.out.println(heater);
    }
}