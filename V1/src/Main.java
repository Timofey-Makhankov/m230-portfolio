public class Main {
    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<Integer>();
        stack.push(3);
        stack.push(6);
        stack.push(9);
        System.out.println(stack);
        stack.pop();
        System.out.println(stack);
        if (stack.hasNext()) {
            stack.pop();
        }
        System.out.println(stack);
        stack.push(5);
        System.out.println(stack);
        stack.remove();
        System.out.println(stack);
    }
}