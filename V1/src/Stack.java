import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Stack<T> implements Iterator<T> {

    private final List<T> list;

    public Stack() {
        list = new ArrayList<>();
    }

    public void pop() {
        list.remove(0);
    }

    public void push(T value) {
        list.add(0, value);
    }

    public int size() {
        return list.size();
    }

    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (T value : list) {
            sb.append(" '");
            sb.append(value);
            sb.append("'");
        }
        sb.append(" ]");
        return sb.toString();
    }

    @Override
    public boolean hasNext() {
        return !isEmpty();
    }

    @Override
    public T next() {
        return list.get(0);
    }

    @Override
    public void remove() {
        list.remove(0);
    }
}
