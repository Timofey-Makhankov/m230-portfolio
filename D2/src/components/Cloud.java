package components;

import java.util.ArrayList;
import java.util.List;

public class Cloud implements Movable {

    private int xPosition;
    private int yPosition;
    private String color;
    private final List<Circle> circles = new ArrayList<>();
    private boolean isVisible;

    public Cloud(){
        xPosition = 50;
        yPosition = 50;
        color = "magenta";

        for (int i = 0; i < 3; i++) {
            circles.add(new Circle());
        }

        circles.get(0).moveHorizontal(40);
        circles.get(2).moveHorizontal(-40);
    }

    /**
     * Change the color. Valid colors are "red", "yellow", "blue", "green",
     * "magenta" and "black".
     */
    public void changeColor(String newColor)
    {
        color = newColor;
        draw();
    }
    /**
     * Draw the cloud with current specifications on screen.
     */
    private void draw()
    {
        if(isVisible) {
            Canvas canvas = Canvas.getCanvas();
            for (Circle circle :
                    circles) {
                circle.makeVisible();
            }
        }
    }

    /**
     * Erase the cloud on screen.
     */
    private void erase()
    {
        if(isVisible) {
            Canvas canvas = Canvas.getCanvas();
            canvas.erase(this);
        }
    }

    @Override
    public void makeVisible() {
        isVisible = true;
        draw();
    }

    @Override
    public void makeInvisible() {
        erase();
        isVisible = false;
    }

    @Override
    public void moveRight() {
        moveHorizontal(20);
    }

    @Override
    public void moveLeft() {
        moveHorizontal(-20);
    }

    @Override
    public void moveUp() {
        moveVertical(-20);
    }

    @Override
    public void moveDown() {
        moveVertical(20);
    }

    @Override
    public void moveHorizontal(int distance) {
        erase();
        //xPosition += distance;
        for (Circle circle :
                circles) {
            circle.moveHorizontal(distance);
        }
        draw();
    }

    @Override
    public void moveVertical(int distance) {
        erase();
        //yPosition += distance;
        for (Circle circle :
                circles) {
            circle.moveVertical(distance);
        }
        draw();
    }

    @Override
    public void slowMoveHorizontal(int distance) {
        int delta;

        if(distance < 0)
        {
            delta = -1;
            distance = -distance;
        }
        else
        {
            delta = 1;
        }

        for(int i = 0; i < distance; i++)
        {
            xPosition += delta;
            for (Circle circle :
                    circles) {
                circle.moveHorizontal(distance);
            }
            //draw();
        }
    }

    @Override
    public void slowMoveVertical(int distance) {
        int delta;

        if(distance < 0)
        {
            delta = -1;
            distance = -distance;
        }
        else
        {
            delta = 1;
        }

        for(int i = 0; i < distance; i++)
        {
            //yPosition += delta;
            for (Circle circle :
                    circles) {
                circle.moveVertical(distance);
            }
            //draw();
        }
    }
}
