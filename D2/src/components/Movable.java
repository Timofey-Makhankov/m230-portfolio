package components;

public interface Movable {
    void makeVisible();
    void makeInvisible();
    void moveRight();
    void moveLeft();
    void moveUp();
    void moveDown();
    void moveHorizontal(int distance);
    void moveVertical(int distance);
    void slowMoveHorizontal(int distance);
    void slowMoveVertical(int distance);
}
