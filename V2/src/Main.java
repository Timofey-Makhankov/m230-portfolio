import football.*;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String userInput = "";
        boolean isRunning = true;
        FootballTeam team = new FootballTeam();
        System.out.println("Commands: show, play, exit, add");
        while (isRunning) {
            userInput = scan.nextLine().toLowerCase();
            switch (userInput) {
                case "exit": {
                    isRunning = false;
                    break;
                }
                case "show": {
                    team.showTeam();
                    break;
                }
                case "play": {
                    team.play();
                    break;
                }
                case "add": {
                    team.addPlayer(addPlayerInput(scan));
                    break;
                }
                default: {
                    System.out.println("Please enter a valid command");
                }
            }
        }
    }

    /**
     * An Input loop for creating a new Player and adding them to the football Team
     * @param scan Scanner for user input
     * @return created Player
     * @see Player
     */
    private static Player addPlayerInput(Scanner scan) {
        System.out.println("What player do you want to add?:");
        System.out.println("Defender\nAttacker\nGoalie");
        String userInput = "";
        while (true) {
            userInput = scan.nextLine().toLowerCase();
            switch (userInput) {
                case "defender": {
                    Defender defender = new Defender();
                    defender.setName("Defender");
                    return defender;
                }
                case "attacker": {
                    Attacker attacker = new Attacker();
                    attacker.setName("Attacker");
                    return attacker;
                }
                case "goalie": {
                    Goalie goalie = new Goalie();
                    goalie.setName("Goalie");
                    return goalie;
                }
                default: {
                    System.out.println("Please Enter a Valid Player");
                    break;
                }
            }
        }
    }
}