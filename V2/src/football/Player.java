package football;

/**
 * Abstract Player Class
 */
public abstract class Player {
    private String name;

    /**
     * Print the Players name to the console
     */
    public void showName(){
        System.out.println("Name: " + this.name);
    }

    /**
     * print a playing action for the player
     */
    public abstract void play();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
