package football;

import java.util.ArrayList;
import java.util.List;

/**
 * A FootBallTeam Class
 */
public class FootballTeam {
    private final List<Player> players = new ArrayList<>();

    public FootballTeam() {
        Goalie goalie = new Goalie();
        goalie.setName("Mark");
        goalie.setBodySize(46.3);
        players.add(goalie);
        for (int i = 0; i < 4; i++) {
            Defender defender = new Defender();
            defender.setName("Defender " + (i + 1));
            players.add(defender);
        }
        for (int i = 0; i < 16; i++) {
            Attacker attacker = new Attacker();
            attacker.setName("Attacker " + (i + 1));
            players.add(attacker);
        }
    }

    /**
     * Add a player to the Team
     * @param player object
     * @see Player
     */
    public void addPlayer(Player player) {
        players.add(player);
    }

    /**
     * Print out the football Team Players to the console
     */
    public void showTeam() {
        for (Player player : players) {
            player.showName();
        }
    }

    /**
     * print out every players play action
     */
    public void play() {
        for (Player player : players) {
            player.play();
        }
    }
}
