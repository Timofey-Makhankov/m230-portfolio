package football;

/**
 * A Defender class that extends the Player class
 * @see Player
 */
public class Defender extends Player{
    /**
     * {@inheritDoc}
     */
    public void play() {
        System.out.printf("%s is helping the Goalie with defending%n", getName());
    }
}
