package football;

/**
 * A Attacker class that extends the Player class
 * @see Player
 */
public class Attacker extends Player{
    /**
     * Print out the jogging exercise to the console
     */
    public void jogTraining(){
        System.out.printf("%s is Jogging for Training%n", getName());
    }

    /**
     * {@inheritDoc}
     */
    public void play() {
        jogTraining();
        System.out.printf("After the Training, %s is ready%n", getName());
    }
}
