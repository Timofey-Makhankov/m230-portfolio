package football;

/**
 * A Goalie Class that extends the Player class
 * @see Player
 */
public class Goalie extends Player{
    private double bodySize;

    /**
     * {@inheritDoc}
     */
    public void play() {
        System.out.println("The Goalie is playing alone");
        System.out.printf("%s is defending the goal%n", getName());
    }

    public double getBodySize() {
        return bodySize;
    }

    public void setBodySize(double bodySize) {
        this.bodySize = bodySize;
    }
}
